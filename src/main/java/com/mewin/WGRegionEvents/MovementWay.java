package com.mewin.WGRegionEvents;

/**
 *
 * @author mewin
 */
public enum MovementWay
{

    MOVE,
    TELEPORT,
    SPAWN,
    DISCONNECT
}
