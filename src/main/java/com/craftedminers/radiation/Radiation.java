package com.craftedminers.radiation;

import com.mewin.WGCustomFlags.WGCustomFlagsPlugin;
import com.mewin.WGRegionEvents.WGRegionEventsListener;
import com.mewin.WGRegionEvents.events.RegionEnterEvent;
import com.mewin.WGRegionEvents.events.RegionLeaveEvent;
import com.sk89q.worldguard.bukkit.WorldGuardPlugin;
import com.sk89q.worldguard.protection.flags.StateFlag;
import com.sk89q.worldguard.protection.flags.StateFlag.State;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class Radiation extends JavaPlugin implements Listener
{

    private WorldGuardPlugin worldGuard;
    private WGCustomFlagsPlugin customFlags;
    private StateFlag radiationFlag = new StateFlag("radiation", true);
    private Map<Player, Long> radiationTime;

    @Override
    public void onDisable()
    {
        if (this.radiationTime != null)
        {
            this.radiationTime.clear();
            this.radiationTime = null;
        }
    }

    @Override
    public void onEnable()
    {
        if (!this.checkWorldGuard())
        {
            this.getLogger().warning("WorldGuard plugin not found! Disabling because why do you need me?");
            this.getPluginLoader().disablePlugin(this);
            return;
        }

        if (!this.checkWGCustomFlags())
        {
            this.getLogger().warning("WGCustomFlags plugin not found! Disabling to prevent any future errors!");
            this.getPluginLoader().disablePlugin(this);
            return;
        }

        this.saveDefaultConfig();

        this.radiationTime = new ConcurrentHashMap<>();

        this.customFlags.addCustomFlag(this.radiationFlag);

        this.getServer().getPluginManager().registerEvents(new WGRegionEventsListener(this, this.worldGuard), this.worldGuard);
        this.getServer().getPluginManager().registerEvents(this, this);

        this.getServer().getScheduler().runTaskTimerAsynchronously(this, new RadiationTimer(this), 20L, 20L);
    }

    @EventHandler
    public void onRegionEnter(RegionEnterEvent event)
    {
        if (event.getRegion().getFlag(this.radiationFlag) == State.ALLOW)
        {
            this.addPlayer(event.getPlayer());
        }
    }

    @EventHandler
    public void onRegionLeave(RegionLeaveEvent event)
    {
        if (event.getRegion().getFlag(this.radiationFlag) == State.ALLOW)
        {
            this.removePlayer(event.getPlayer());
        }
    }

    private boolean checkWorldGuard()
    {
        Plugin plugin = getServer().getPluginManager().getPlugin("WorldGuard");

        if (plugin != null)
        {
            worldGuard = (WorldGuardPlugin) plugin;
            return true;
        }

        return false;
    }

    private boolean checkWGCustomFlags()
    {
        Plugin plugin = getServer().getPluginManager().getPlugin("WGCustomFlags");

        if (plugin != null)
        {
            customFlags = (WGCustomFlagsPlugin) plugin;
            return true;
        }

        return false;
    }

    private boolean addPlayer(Player player)
    {
        if (!this.radiationTime.containsKey(player))
        {
            this.radiationTime.put(player, System.currentTimeMillis());
            return true;
        }

        return false;
    }

    private boolean removePlayer(Player player)
    {
        if (this.radiationTime.containsKey(player))
        {
            this.radiationTime.remove(player);

            if (player.hasPotionEffect(PotionEffectType.CONFUSION))
            {
                player.removePotionEffect(PotionEffectType.POISON);
                player.removePotionEffect(PotionEffectType.SLOW);
                player.removePotionEffect(PotionEffectType.CONFUSION);
                player.addPotionEffect(new PotionEffect(PotionEffectType.CONFUSION, 100, 0));
            }

            return true;
        }

        return false;
    }

    public class RadiationTimer implements Runnable
    {

        private final Radiation plugin;
        private final int immune;
        private final int nauseaTime;
        private final int nauseaLevel;
        private final int slownessTime;
        private final int slownessLevel;
        private final int poisonTime;
        private final int poisonLevel;
        private final boolean kill;

        public RadiationTimer(Radiation plugin)
        {
            this.plugin = plugin;
            this.immune = plugin.getConfig().getInt("immune");
            this.nauseaTime = plugin.getConfig().getInt("nausea");
            this.nauseaLevel = plugin.getConfig().getInt("nausea-level");
            this.slownessTime = plugin.getConfig().getInt("slowness");
            this.slownessLevel = plugin.getConfig().getInt("slowness-level");
            this.poisonTime = plugin.getConfig().getInt("poison");
            this.poisonLevel = plugin.getConfig().getInt("poison-level");
            this.kill = plugin.getConfig().getBoolean("kill");
        }

        @Override
        public void run()
        {
            long current = System.currentTimeMillis();

            for (Player player : this.plugin.radiationTime.keySet())
            {
                if (!player.hasPermission("radiation.hazmat"))
                {
                    long time = current - this.plugin.radiationTime.get(player);

                    if ((time > (this.immune * 1000)) && (time <= ((this.immune + this.nauseaTime) * 1000)))
                    {
                        if (!player.hasPotionEffect(PotionEffectType.CONFUSION))
                        {
                            player.sendMessage(String.format("%s** You are in a radiation area! You start to feel nauseous...", ChatColor.GREEN));
                            player.addPotionEffect(new PotionEffect(PotionEffectType.CONFUSION,
                                                                    (this.nauseaTime + this.slownessTime + this.poisonTime) * 20,
                                                                    this.nauseaLevel - 1));
                        }
                    }
                    else if ((time > ((this.immune + this.nauseaTime) * 1000)) && (time <= ((this.immune + this.nauseaTime + this.slownessTime) * 1000)))
                    {
                        if (!player.hasPotionEffect(PotionEffectType.SLOW))
                        {
                            player.addPotionEffect(new PotionEffect(PotionEffectType.SLOW,
                                                                    (this.slownessTime + this.poisonTime) * 20,
                                                                    this.slownessLevel - 1));
                        }
                    }
                    else if ((time > ((this.immune + this.nauseaTime + this.slownessTime) * 1000)) && (time <= ((this.immune + this.nauseaTime + this.slownessTime + this.poisonTime) * 1000)))
                    {
                        if (!player.hasPotionEffect(PotionEffectType.POISON))
                        {
                            player.addPotionEffect(new PotionEffect(PotionEffectType.POISON,
                                                                    this.poisonTime * 20,
                                                                    this.poisonLevel - 1));
                        }
                    }
                    else if ((time >= ((this.immune + this.nauseaTime + this.slownessTime + this.poisonTime) * 1000)) && this.kill)
                    {
                        player.setHealth(0);
                    }
                }
                else
                {
                    this.plugin.removePlayer(player);
                }
            }
        }
    }
}
